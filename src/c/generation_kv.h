#pragma once
#include <string.h>
#include <inttypes.h>
#include "generation.h"

typedef struct {
	uint32_t msg_id; /* We store messages on 4 bytes */
	uint8_t coef_value;
} genkv_coef_t;

typedef struct {
	generation_t gen;
	uint16_t     known_ids_count;
	uint16_t     known_ids_alloc;
	uint32_t*    known_ids;
	uint8_t*     coef_buffer;
} genkv_t;

void genkv_print(genkv_t* genkv);
int genkv_init(genkv_t* genkv, ctx_t* ctx, uint64_t lines_alloc);
uint8_t genkv_add(genkv_t* genkv, genkv_coef_t* new_coef, uint16_t new_coef_n, unsigned char* new_val, uint16_t new_val_n);
uint8_t genkv_add_local(genkv_t* genkv, genkv_coef_t* new_coef, uint16_t new_coef_n, unsigned char* new_val, uint16_t new_val_n);
void genkv_recode(genkv_t* genkv, genkv_coef_t* out_coef, unsigned char* out_val);
uint8_t** genkv_deliver(genkv_t* genkv, uint16_t* out_n);
unsigned int genkv_rank(genkv_t* genkv);
int genkv_get_msg_id_pos(genkv_t* genkv, uint32_t msg_id);
void genkv_free(genkv_t* genkv);
