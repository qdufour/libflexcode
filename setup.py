from setuptools import setup

setup(
    name='pyflexcode',
    version='0.1',
    description='A flexible network coding library',
    url='https://gitlab.inria.fr/qdufour/libflexcode',
    author='Quentin Dufour',
    author_email='quentin@dufour.io',
    license='GPLv3',
    setup_requires=["cffi>=1.0.0"],
    packages=['pyflexcode'],
    cffi_modules=["pyflexcode/build.py:ffibuilder"],
    install_requires=["cffi>=1.0.0"],
)

