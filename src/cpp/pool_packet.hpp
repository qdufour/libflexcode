extern "C" {
#include "../c/pool.h"
}

class PoolPacket {
  protected:
  public:
    pool_packet_t cpkt;
    PoolPacket();
    PoolPacket(uint32_t gid, unsigned char* msg, uint16_t len);
    ~PoolPacket();
};
