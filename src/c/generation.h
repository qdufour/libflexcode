#pragma once

/*
 * === GENERATION ===
 * Here we have a special case of network coding, as we want to have messages and coefficients of various sizes
 * We don't know before the end of the execution what will be the number of messages in the matrix, the final length of coefficients nor the final length of messages
 * For more information on Network Coding, please read "Network Coding: An Instant Primer"
 * https://infoscience.epfl.ch/record/58339/files/rt.pdf
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <search.h>
#include <inttypes.h>
#include "gf256.h"

typedef struct {
	ctx_t* ctx;
	unsigned char** coef; /* coef part of the matrix */
	unsigned char** val; /* values part of the matrix */
	unsigned char* coef_buffer; /* handle data to be inserted */
	unsigned char* val_buffer; /* idem */
	uint8_t* delivered; /* track delivered messages */
	uint8_t** val_deliver_buffer; /* used to return val to user */
	unsigned int coef_allocated;
	unsigned int coef_size;
	unsigned int val_allocated;
	unsigned int val_size;
	unsigned int rows_allocated;
	unsigned int rows_size;
	unsigned int cols_size;
	unsigned int real_rank;
	unsigned int delivered_count;
} generation_t;

void gen_print(generation_t* gen);
void gen_init(generation_t* gen, ctx_t* ctx, unsigned int lines_alloc);
unsigned int gen_add(generation_t* gen, unsigned char* new_coef, unsigned int new_coef_n, unsigned char* new_val, unsigned int new_val_n);
void gen_recode(generation_t* gen, unsigned char* out_coef, unsigned char* out_val);
uint8_t** gen_deliver(generation_t* gen, uint16_t* out_n);
unsigned int gen_rank(generation_t* gen);
void gen_free(generation_t* gen);
