import os, re
from cffi import FFI
ffibuilder = FFI()

src_dir = os.path.dirname(os.path.abspath(__file__)) + "/../src/c/"

code = ""
for f in ["gf256.h", "gf256.c", "generation.h", "generation.c", "generation_kv.h", "generation_kv.c", "dict.h", "dict.c", "pool.h", "pool.c"]:
    with open(src_dir + f, "r") as fd:
        code += fd.read()

# ugly hack
code = re.sub(r'#include\s+".*"', "", code)
code = re.sub(r'#pragma\s+once', "", code)

ffibuilder.set_source("_pyflexcode", code, libraries=[])

ffibuilder.cdef("""
typedef struct {
	unsigned char pol;
	unsigned char generator;
	unsigned char exp[256];
	unsigned char log[256];
        FILE* trace_fd;
} ctx_t;

typedef struct {
	ctx_t* ctx;
	unsigned char** coef;
	unsigned char** val;
	unsigned char* coef_buffer; 
	unsigned char* val_buffer;
	uint8_t* delivered; /* track delivered messages */
	uint8_t** val_deliver_buffer; /* used to return val to user */
	unsigned int coef_allocated;
	unsigned int coef_size;
	unsigned int val_allocated;
	unsigned int val_size;
	unsigned int rows_allocated;
	unsigned int rows_size;
	unsigned int cols_size;
} generation_t;

void ffinit(ctx_t* ctx, unsigned char pol);
void gen_print(generation_t* gen);
void gen_init(generation_t* gen, ctx_t* ctx, unsigned int lines_alloc);
unsigned int gen_add(generation_t* gen, unsigned char* new_coef, unsigned int new_coef_n, unsigned char* new_val, unsigned int new_val_n);
void gen_recode(generation_t* gen, unsigned char* out_coef, unsigned char* out_val);
uint8_t** gen_deliver(generation_t* gen, uint16_t* out_n);
unsigned int gen_rank(generation_t* gen);
void gen_free(generation_t* gen);

typedef struct {
	uint32_t msg_id; /* We store messages on 4 bytes */
	uint8_t coef_value;
} genkv_coef_t;

typedef struct {
	generation_t gen;
	uint16_t     known_ids_count;
	uint16_t     known_ids_alloc;
	uint32_t*    known_ids;
	uint8_t*     coef_buffer;
} genkv_t;

void genkv_print(genkv_t* genkv);
int genkv_init(genkv_t* genkv, ctx_t* ctx, uint64_t lines_alloc);
uint8_t genkv_add(genkv_t* genkv, genkv_coef_t* new_coef, uint16_t new_coef_n, unsigned char* new_val, uint16_t new_val_n);
uint8_t genkv_add_local(genkv_t* genkv, genkv_coef_t* new_coef, uint16_t new_coef_n, unsigned char* new_val, uint16_t new_val_n);
void genkv_recode(genkv_t* genkv, genkv_coef_t* out_coef, unsigned char* out_val);
uint8_t** genkv_deliver(genkv_t* genkv, uint16_t* out_n);
unsigned int genkv_rank(genkv_t* genkv);
void genkv_free(genkv_t* genkv);

typedef struct hsearch_data { ...; };
typedef struct entry { char* key; void* data; } ENTRY;

typedef struct {
	struct hsearch_data htab;
	ENTRY* entries;
	size_t entries_size;
	size_t entries_alloc;
} dict_t;

typedef struct {
	uint32_t gid; /* generation id */
	uint8_t ttl;
	uint16_t coefs_count;
	genkv_coef_t* coefs;
	uint16_t vals_count;
	uint8_t* vals;
} pool_packet_t;

void pool_packet_deep_cpy(pool_packet_t* dest, pool_packet_t* src);
void pool_packet_free(pool_packet_t* pp);

/* Function pointers types */
typedef void (*deliver_fx_t)(void*, unsigned char*, uint16_t);
typedef void (*send_fx_t)(void*, pool_packet_t*, uint8_t);

extern "Python" void my_deliver_cb(void*, unsigned char*, uint16_t);
extern "Python" void my_send_cb(void*, pool_packet_t*, uint8_t);

typedef struct {
	void* handle;
	uint8_t initial_ttl;
	uint32_t current_generation;
	deliver_fx_t deliver;
	send_fx_t send;
	uint8_t fanout;
	uint16_t max_rank;
	uint16_t split;
	dict_t generations;
	pool_packet_t* packet_buffer;
	uint16_t* gen_hist;
	uint16_t gen_hist_size;
	uint16_t gen_hist_cursor;
	uint16_t gen_last_action;
	ctx_t ctx;
} pool_t;

void pool_init(pool_t* p, void* handle, deliver_fx_t deliver, send_fx_t send, uint8_t fanout, uint16_t max_rank, uint16_t split, uint16_t gen_hist_size, uint8_t pol, uint8_t ttl);
void pool_broadcast(pool_t* p, unsigned char* msg, uint16_t msg_len);
void pool_broadcast_msgid(pool_t* p, uint32_t msg_id, unsigned char* msg, uint16_t msg_len);
int  pool_receive(pool_t* p, pool_packet_t* pkt);
void pool_recode(pool_t* p, pool_packet_t* pkt, uint32_t gid, uint8_t ttl);
genkv_t* pool_get_gen(pool_t* p, uint32_t gid);
void pool_free(pool_t* p);
""")

if __name__ == '__main__':
    ffibuilder.compile(verbose=True)
