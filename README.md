# libflexcode

libflexcode is a network coding library which work even if the following information are not known / stable:
  * message count in a generation
  * messages size
  * coefficients size

## Compilation

On Linux:

```
mkdir build && cd build
cmake -DCMAKE_BUILD_TYPE=Release ..
cmake --build . --config Release
```

If you want symbols:

```
cmake --build . --config Debug 
```

or

```
cmake --build . --config RelWithDebInfo
```

If you are not root, you can change the prefix like that:

```
cmake -DCMAKE_INSTALL_PREFIX:PATH=~/.local ..
cmake --build . --target install
```

## pyflexcode

pyflexcode is a pretty raw python binding to libflexcode.
To compile / install the package, you should run in the repository root directory:

```
pip install .
```

## flex-replay tool

Used to replay some traces to debug or benchmark the implementation.

Example:

```
./build/flex-replay traces/simple-tests.log
```

Use it to benchmark on a bigger file:

```
gzip -cd traces/huge-epto-100.log.gz > traces/huge-epto-100.log
/usr/bin/time ./build/flex-replay ./traces/huge-epto-100.log
```
