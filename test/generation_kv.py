import pyflexcode

ctx = pyflexcode.ffi.new("ctx_t*")
gen = pyflexcode.ffi.new("genkv_t*")
coefs = pyflexcode.ffi.new("genkv_coef_t[]", 10)
vals = pyflexcode.ffi.new("uint8_t[]", 12)

pyflexcode.lib.ffinit(ctx, 27)
pyflexcode.lib.genkv_init(gen, ctx, 1)
pyflexcode.lib.genkv_print(gen)

for i in range(10):
    coefs[i].msg_id = i
    coefs[i].coef_value = 1

for i in range(12):
    vals[i] = 12-i

print coefs[9].msg_id
pyflexcode.lib.genkv_add(gen, coefs, 10, vals, 12)
pyflexcode.lib.genkv_print(gen)

for i in range(10):
    coefs[i].msg_id = i
    coefs[i].coef_value = i

for i in range(12):
    vals[i] = 100+i

pyflexcode.lib.genkv_add(gen, coefs, 10, vals, 12)
pyflexcode.lib.genkv_print(gen)

coefs[0].coef_value = 1
vals[0] = 1
pyflexcode.lib.genkv_add(gen, coefs, 1, vals, 1)
pyflexcode.lib.genkv_print(gen)

s = pyflexcode.ffi.new("uint16_t*")
out = pyflexcode.lib.genkv_deliver(gen, s)

print "res: ", out[0][1]
pyflexcode.lib.genkv_print(gen)
