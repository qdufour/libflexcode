#include "pool.hpp"
#include <iostream>

Pool::Pool(): inited(false) {}

void Pool::init(void* handle, deliver_fx_t deliver, send_fx_t send, uint8_t fanout, uint16_t max_rank, uint16_t split, uint16_t gen_hist_size, uint8_t pol, uint8_t ttl) {
  pool_init(&cpool, handle, deliver, send, fanout, max_rank, split, gen_hist_size, pol, ttl);
  inited = true;
}

Pool::~Pool() {
  if (inited) {
    pool_free(&cpool);
  }
}

void Pool::broadcast(unsigned char* msg, uint16_t msg_len) {
  pool_broadcast(&cpool, msg, msg_len);
}

void Pool::broadcast(uint32_t msg_id, unsigned char* msg, uint16_t msg_len) {
  pool_broadcast_msgid(&cpool, msg_id, msg, msg_len);
}

bool Pool::receive(pool_packet_t* pkt) {
  return !pool_receive(&cpool, pkt);
}

genkv_t* Pool::get_gen(uint32_t gen) {
  return pool_get_gen(&cpool, gen);  
}

uint32_t Pool::get_message_generation(uint32_t msg_id) {
  uint32_t current_gen = current_generation();
  for (uint32_t i = 0; i < current_gen; i++) {
    genkv_t* gen = get_gen(i);  
    if (gen == NULL) continue;
    if (genkv_get_msg_id_pos(gen, msg_id) != -1) {
      return i;
    }
  }
  return UINT32_MAX;
}

std::vector<unsigned int> Pool::stat_generations_size() {
  std::vector<unsigned int> res;
  for (uint32_t i = 0; i < cpool.generations.entries_size; i++) {
    res.push_back(genkv_rank((genkv_t*)cpool.generations.entries[i].data));
  }
  return res;
}

void Pool::recode(pool_packet_t* pkt, uint32_t gid, uint8_t ttl) {
  return pool_recode(&cpool, pkt, gid, ttl);
}

uint32_t Pool::current_generation() {
  return cpool.current_generation;
}
