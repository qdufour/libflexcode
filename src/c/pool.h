#pragma once
#define _GNU_SOURCE 1
#include <search.h>
#include <inttypes.h>
#include <string.h>
#include <unistd.h>
#include <sys/syscall.h>
#include "dict.h"
#include "generation_kv.h"
#include "gf256.h"

#define POOL_LINES_ALLOC 20

typedef struct {
	uint32_t gid; /* generation id */
	uint8_t ttl;
	uint16_t coefs_count;
	genkv_coef_t* coefs;
	uint16_t vals_count;
	uint8_t* vals;
} pool_packet_t;

void pool_packet_deep_cpy(pool_packet_t* dest, pool_packet_t* src);
void pool_packet_free(pool_packet_t* pp);

/* Function pointers types */
typedef void (*deliver_fx_t)(void*, unsigned char*, uint16_t);
typedef void (*send_fx_t)(void*, pool_packet_t*, uint8_t);

typedef struct {
	void* handle;
	uint8_t initial_ttl;
	uint32_t current_generation;
	deliver_fx_t deliver;
	send_fx_t send;
	uint8_t fanout;
	uint16_t max_rank;
	uint16_t split;
	dict_t generations;
	pool_packet_t* packet_buffer;
	uint16_t* gen_hist;
	uint16_t gen_hist_size;
	uint16_t gen_hist_cursor;
	uint16_t gen_last_action;
	ctx_t ctx;
} pool_t;

void pool_init(pool_t* p, void* handle, deliver_fx_t deliver, send_fx_t send, uint8_t fanout, uint16_t max_rank, uint16_t split, uint16_t gen_hist_size, uint8_t pol, uint8_t ttl);
void pool_broadcast(pool_t* p, unsigned char* msg, uint16_t msg_len);
void pool_broadcast_msgid(pool_t* p, uint32_t msg_id, unsigned char* msg, uint16_t msg_len);
int  pool_receive(pool_t* p, pool_packet_t* pkt);
void pool_recode(pool_t* p, pool_packet_t* pkt, uint32_t gid, uint8_t ttl);
genkv_t* pool_get_gen(pool_t* p, uint32_t gid);
void pool_free(pool_t* p);
