#include "generation_kv.h"

void genkv_print(genkv_t* genkv) {
	printf("generation_kv(known_ids: %d/%d) {\n", genkv->known_ids_count, genkv->known_ids_alloc);
	for (int i = 0; i < genkv->known_ids_count; i++) {
		printf("%d\t", genkv->known_ids[i]);
	}
	printf("\n}\n");
	gen_print(&(genkv->gen));
}

int genkv_init(genkv_t* genkv, ctx_t* ctx, uint64_t lines_alloc) {
	gen_init(&(genkv->gen), ctx, lines_alloc);


	genkv->known_ids_count = 0;
	genkv->known_ids_alloc = 1;
	genkv->known_ids = malloc(genkv->known_ids_alloc * sizeof(uint32_t));
	genkv->coef_buffer = malloc(genkv->known_ids_alloc * sizeof(uint8_t));
	
	if (genkv->known_ids == NULL) return -1;
	return 0;
}

void __genkv_new_msgid(genkv_t* genkv, genkv_coef_t* coef) {
	if (genkv->known_ids_count >= genkv->known_ids_alloc) {
		uint16_t new_size = 2 * genkv->known_ids_alloc;
		genkv->known_ids = realloc(genkv->known_ids, new_size * sizeof(uint32_t)); 
		genkv->coef_buffer = realloc(genkv->coef_buffer, new_size * sizeof(uint8_t));
		genkv->known_ids_alloc = new_size;
	}
	uint16_t pos = genkv->known_ids_count;
	genkv->known_ids[pos] = coef->msg_id;
	genkv->coef_buffer[pos] = coef->coef_value;
	genkv->known_ids_count++;
}

int genkv_get_msg_id_pos(genkv_t* genkv, uint32_t msg_id) {
	int position = -1;
	for (uint16_t j = 0; j < genkv->known_ids_count; j++) {
		if (msg_id == genkv->known_ids[j]) {
			position = j;
			break;
		}
	}
	return position;
}

uint8_t genkv_add(genkv_t* genkv, genkv_coef_t* new_coef, uint16_t new_coef_n, unsigned char* new_val, uint16_t new_val_n) {
	/* Zeroing coefficient vector  */
	memset(genkv->coef_buffer, 0, genkv->known_ids_count);

	/* Set coefficients */
	for (uint16_t i = 0; i < new_coef_n; i++) {
		int position = genkv_get_msg_id_pos(genkv, new_coef[i].msg_id);
		if (position == -1) __genkv_new_msgid(genkv, &(new_coef[i]));
		else genkv->coef_buffer[position] = new_coef[i].coef_value;
	}

	return gen_add(&(genkv->gen), genkv->coef_buffer, genkv->known_ids_count, new_val, new_val_n);
}

void genkv_recode(genkv_t* genkv, genkv_coef_t* out_coef, unsigned char* out_val) {
	gen_recode(&(genkv->gen), genkv->coef_buffer, out_val);
	for (uint16_t i = 0; i < genkv->known_ids_count; i++) {
		out_coef[i].msg_id = genkv->known_ids[i];
		out_coef[i].coef_value = genkv->coef_buffer[i];
	}
}


uint8_t** genkv_deliver(genkv_t* genkv, uint16_t* out_n) {
	return gen_deliver(&(genkv->gen), out_n);
}

unsigned int genkv_rank(genkv_t* genkv) {
	return gen_rank(&(genkv->gen));
}

uint8_t genkv_add_local(genkv_t* genkv, genkv_coef_t* new_coef, uint16_t new_coef_n, unsigned char* new_val, uint16_t new_val_n) {
	uint8_t res = genkv_add(genkv, new_coef, new_coef_n, new_val, new_val_n);
	for (uint16_t i = 0; i < new_coef_n; i++) {
		int position = genkv_get_msg_id_pos(genkv, new_coef[i].msg_id);
		genkv->gen.delivered[position] = 1;
	}
	return res;
}

void genkv_free(genkv_t* genkv) {
	free(genkv->known_ids);
	free(genkv->coef_buffer);
	gen_free(&(genkv->gen));
}
