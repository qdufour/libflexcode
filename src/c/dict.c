#include "dict.h"

void dict_init(dict_t* d, size_t max_size) {
	d->htab = (struct hsearch_data){0};
	hcreate_r(max_size, &(d->htab));
	
	d->entries_size = 0;
	d->entries_alloc = 16;
	d->entries = malloc(d->entries_alloc * sizeof(ENTRY));
}

/* Data will be managed by the dict and not anymore by the caller */
int dict_add(dict_t* d, char *key, void *data) {
	ENTRY e = { .key = key, .data = data };
	ENTRY* ret_val;

	int ret = hsearch_r(e, ENTER, &ret_val, &(d->htab));
	if (ret == 0) {
		free(key);
		free(data);
	} else {
		if (d->entries_size == d->entries_alloc) {
			d->entries_alloc = 2 * d->entries_alloc;
			d->entries = realloc(d->entries, d->entries_alloc * sizeof(ENTRY));
		}
		d->entries[d->entries_size] = e;
		d->entries_size++;
	}

	return ret;
}

void* dict_get(dict_t* d, char *key) {
	ENTRY search = { .key = key, .data = NULL };
	ENTRY* retval;
	int ret = hsearch_r(search, FIND, &retval, &(d->htab));
	if (ret == 0) return NULL;
	return retval->data;
}

void dict_free(dict_t* d) {
	hdestroy_r(&(d->htab));
	for (unsigned int i = 0; i < d->entries_size; i++) {
		free(d->entries[i].key);
		free(d->entries[i].data);
	}
	free(d->entries);
}

