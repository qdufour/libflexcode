#pragma once
#define _GNU_SOURCE 1
#include <stdlib.h>
#include <search.h>

typedef struct {
	struct hsearch_data htab;

	/* @FIXME Could be replaced by a linked list */
	ENTRY* entries;
	size_t entries_size;
	size_t entries_alloc;
} dict_t;


void dict_init(dict_t* d, size_t max_size);
int dict_add(dict_t* d, char *key, void *data);
void* dict_get(dict_t* d, char *key);
void dict_free(dict_t* d);
