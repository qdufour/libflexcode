#include "generation.h"

void gen_print(generation_t* gen) {
	printf("generation(ctx.pol=%d, ctx.gen=%d, rows=%d/%d, cols=%d, cols_coef=%d/%d, cols_val=%d/%d) {\n",
			gen->ctx->pol, gen->ctx->generator, gen->rows_size, gen->rows_allocated, gen->cols_size, gen->coef_size, gen->coef_allocated, gen->val_size, gen->val_allocated);

	for (unsigned int i = 0; i < gen->rows_size; i++) {
		if (gen->coef[i] == NULL) {
			printf("\t---\n");
			continue;
		}
		printf("\t");
		for (unsigned int j = 0; j < gen->coef_size; j++) {
			printf("%d,\t", gen->coef[i][j]);
		}
		for (unsigned int j = 0; j < gen->val_size; j++) {
			printf("%d,\t", gen->val[i][j]);
		}

		printf("\n");
	}

	/* BUFFER */
	printf("B:\t");
	for (unsigned int j = 0; j < gen->coef_size; j++) {
		printf("%d,\t", gen->coef_buffer[j]);
	}
	for (unsigned int j = 0; j < gen->val_size; j++) {
		printf("%d,\t", gen->val_buffer[j]);
	}

	/* DELIVERED */
	printf("\nD:\t");
	for (unsigned int j = 0; j < gen->coef_size; j++) {
		printf("%d,\t", gen->delivered[j]);
	}
	printf("\n}\n");
}

void gen_init(generation_t* gen, ctx_t* ctx, unsigned int lines_alloc) {
	#ifdef TRACE
	char buffer[2048];
	sprintf(buffer, "gen_init %p %p %u\n", (void *) gen, (void *) ctx, lines_alloc);
	fwrite(buffer, sizeof(char), strlen(buffer), ctx->trace_fd);
	#endif

	gen->ctx = ctx;
	gen->coef = malloc(lines_alloc * sizeof(unsigned char*));
	gen->val = malloc(lines_alloc * sizeof(unsigned char*));
	gen->coef_buffer = malloc(1 * sizeof(unsigned char));
	gen->val_buffer = malloc(1 * sizeof(unsigned char));
	gen->delivered = malloc(1 * sizeof(uint8_t));
	gen->val_deliver_buffer = malloc(1 * sizeof(uint8_t*));
	gen->coef_allocated = 0;
	gen->coef_size = 0;
	gen->val_allocated = 0;
	gen->val_size = 0;
	gen->rows_allocated = lines_alloc;
	gen->rows_size = 0;
	gen->cols_size = 0;
	gen->real_rank = 0;
	gen->delivered_count = 0;
}

void __gen_expand(generation_t* gen, unsigned int lines, unsigned int new_coef_n, unsigned int new_val_n) {
	/* Expand lines allocation */
	if (lines > gen->rows_allocated) {
		unsigned int new_size = MAX(gen->rows_allocated * 2, lines);
		gen->coef = realloc(gen->coef, new_size * sizeof(unsigned char*));
		gen->val  = realloc(gen->val, new_size * sizeof(unsigned char*));
		gen->rows_allocated = new_size;
	}

	/* Expand coef cols allocation */
	if (new_coef_n > gen->coef_allocated) {
		unsigned int new_size = MAX(new_coef_n, 2 * gen->coef_allocated);
		for (unsigned int i = 0; i < gen->rows_size; i++) {
			if (gen->coef[i] == NULL) continue;
			gen->coef[i] = realloc(gen->coef[i], new_size * sizeof(unsigned char));
		}
		/* Expand coef buffer, delivered and val_deliver_buffer */
		gen->coef_buffer = realloc(gen->coef_buffer, new_size * sizeof(unsigned char));
		gen->delivered = realloc(gen->delivered, new_size * sizeof(uint8_t));
		gen->val_deliver_buffer = realloc(gen->val_deliver_buffer, new_size * sizeof(uint8_t*));
		gen->coef_allocated = new_size;
	}

	/* Expand val cols allocation */
	if (new_val_n > gen->val_allocated) {
		unsigned int new_size = MAX(new_val_n, 2*gen->val_allocated);
		for (unsigned int i = 0; i < gen->rows_size; i++) {
			if (gen->val[i] == NULL) continue;
			gen->val[i] = realloc(gen->val[i], new_size * sizeof(unsigned char));
		}
		/* Expand val buffer */
		gen->val_buffer = realloc(gen->val_buffer, new_size * sizeof(unsigned char));
		gen->val_allocated = new_size;
	}
}

void __gen_pad(generation_t* gen, unsigned int new_coef_n, unsigned int new_val_n) {
	/* Pad rows with NULL pointers - don't know how to do with memset */
	for (unsigned int i = gen->rows_size; i < new_coef_n; i++) {
        	gen->coef[i] = NULL;
		gen->val[i] = NULL;
	}

	gen->rows_size = new_coef_n;

	int pad_coef = new_coef_n > gen->coef_size;
	int pad_value = new_val_n > gen->val_size;
	if (!pad_coef && !pad_value) return;

	for (unsigned int i = 0; i < gen->rows_size; i++) {
		if (gen->coef[i] == NULL) continue;
		
		/* Pad coef with zero */
		if (pad_coef) {
			memset(&(gen->coef[i][gen->coef_size]), 0, new_coef_n - gen->coef_size);
		}
	
		/* Pad values with last char */
		/* @FIXME does it really work in every cases? */
		if (pad_value) {
			/* 
			 * We pad each message with its last character,
			 * so if your message is null terminated before encoding (end with a byte 0x00),
			 * once decoded you'll have a series of null character.
			 * If your message is length bounded, you can just throw the remaining characters.
			 * You can't put a random/hard coded byte as I think it will really break things
			 * when recoding on different nodes with different coef.
			 */
			unsigned char last = gen->val[i][gen->val_size - 1];
			memset(&(gen->val[i][gen->val_size]), last, new_val_n - gen->val_size);
		}
	}

	if (pad_coef) {
		memset(&(gen->delivered[gen->coef_size]), 0, new_coef_n - gen->coef_size);
		gen->coef_size = new_coef_n;
	}
	if (pad_value)
		gen->val_size = new_val_n;

	gen->cols_size = gen->coef_size + gen->val_size;
}

void __gen_alloc_line(generation_t* gen, unsigned int row) {
	gen->coef[row] = malloc(gen->coef_allocated * sizeof(unsigned char));
	gen->val[row] = malloc(gen->val_allocated * sizeof(unsigned char));
	if (gen->coef[row] == NULL || gen->val[row] == NULL) {
		printf("malloc for __gen_alloc_line failed\n");
		exit(1);
	}
}

void __gen_insert(generation_t* gen, unsigned char* new_coef, unsigned char* new_val, unsigned int new_val_n) {
	if (gen->coef_size > 0) memcpy(gen->coef_buffer, new_coef, gen->coef_size);
	if (new_val_n > 0) memcpy(gen->val_buffer, new_val, new_val_n);
	
	/* We pad the value if needed */
	if (new_val_n < gen->val_size) {
		unsigned char last = new_val_n == 0 ? 0 : gen->val_buffer[new_val_n - 1];
		memset(&(gen->val_buffer[new_val_n]), last, gen->val_size - new_val_n);
	}
}

/*
 * We use a gauss jordan elimination to obtain a reduced row echelon form matrix
 * Indeed, we want to solve a linear equation system here
 */
unsigned int  __gen_solve(generation_t* gen) {
	/* === Forward substitution === */
	for (unsigned int i = 0; i < gen->rows_size; i++) {
		if (gen->coef[i] == NULL) continue;

		unsigned char c = gen->coef_buffer[i];
		if (gen->coef[i][i] != 0 && c != 0) {
			/* gen->buffer / c + gen[i] (first for coefficients, next for values */
			for (unsigned int j = 0; j < gen->coef_size; j++) {
				unsigned char t = ffdiv(gen->ctx, gen->coef_buffer[j], c);
				gen->coef_buffer[j] = ffadd(t, gen->coef[i][j]);
			}
			
			for (unsigned int j = 0; j < gen->val_size; j++) {
				unsigned char t = ffdiv(gen->ctx, gen->val_buffer[j], c);
				gen->val_buffer[j] = ffadd(t, gen->val[i][j]);
			}
		}
	}
	/* === Find the pivot element === */
	/* Define pivot at a value that can't be reached */
	/* If no useful information is contained, the coef buffer is reduced to 0 */
	unsigned int pivotposition = gen->coef_size;
	for (unsigned int i = 0; i < gen->coef_size; i++) {
		if (gen->coef_buffer[i] != 0) {
			pivotposition = i;
			break;
		}
	}
	/* The packet contains only 0 coefficients */
	if (pivotposition == gen->coef_size) return 1;

	/* === Put the normalized packet in place === */
	unsigned char pivot = gen->coef_buffer[pivotposition];
	__gen_alloc_line(gen, pivotposition);
	for (unsigned int j = 0; j < gen->coef_size; j++) {
		gen->coef[pivotposition][j] = ffdiv(gen->ctx, gen->coef_buffer[j], pivot);
	}
	for (unsigned int j = 0; j < gen->val_size; j++) {
		gen->val[pivotposition][j] = ffdiv(gen->ctx, gen->val_buffer[j], pivot);
	}

	/* === Backward substitution === */
	for (unsigned int i = 0; i < gen->rows_size; i++) {
		if (gen->coef[i] == NULL) continue;

		unsigned char m = gen->coef[i][pivotposition];
		if (gen->coef[i][i] != 0 && i != pivotposition) {
			for (unsigned int j = 0; j < gen->coef_size; j++) {
				unsigned char t = ffmul(gen->ctx, gen->coef[pivotposition][j], m);
				gen->coef[i][j] = ffsub(gen->coef[i][j], t);
			}
			for (unsigned int j = 0; j < gen->val_size; j++) {
				unsigned char t = ffmul(gen->ctx, gen->val[pivotposition][j], m);
				gen->val[i][j] = ffadd(gen->val[i][j], t);
			}
		}
	} 
	return 0;
}

unsigned int gen_add(generation_t* gen, unsigned char* new_coef, unsigned int new_coef_n, unsigned char* new_val, unsigned int new_val_n) {
	#ifdef TRACE
	char buffer[2048];
	sprintf(buffer, "gen_add %p %u %u", (void *) gen, new_coef_n, new_val_n);
	for (unsigned int i = 0; i < new_coef_n; i++) {
		sprintf(buffer + strlen(buffer), " %u", new_coef[i]);
	}
	for (unsigned int i = 0; i < new_val_n; i++) {
		sprintf(buffer + strlen(buffer), " %u", new_val[i]);
	}
	sprintf(buffer + strlen(buffer), "\n");
	fwrite(buffer, sizeof(char), strlen(buffer), gen->ctx->trace_fd);
	#endif

	__gen_expand(gen, new_coef_n, new_coef_n, new_val_n);
	__gen_pad(gen, new_coef_n, new_val_n);
	__gen_insert(gen, new_coef, new_val, new_val_n);
	unsigned int useless = __gen_solve(gen);
	if (!useless) gen->real_rank++;
	return useless;
}

/*
 * User must pass 2 arrays (out_coef and out_val) of size at least gen->coef_size and gen->val_size respectively.
 */
void gen_recode(generation_t* gen, unsigned char* out_coef, unsigned char* out_val) {
	#ifdef TRACE
	char buffer[2048];
	sprintf(buffer, "gen_recode %p %u %u\n", (void *) gen, gen->coef_size, gen->val_size);
	fwrite(buffer, sizeof(char), strlen(buffer), gen->ctx->trace_fd);
	#endif
	
	unsigned char* temp_coef = malloc(gen->coef_size * sizeof(unsigned char));
	if (temp_coef == NULL) {
		printf("malloc failed in gen_recode for temp_coef\n");
		exit(1);
	}

	/* Generate random coefficients */
	for (unsigned int i = 0; i < gen->coef_size; i++) {
		temp_coef[i] = (unsigned char) rand();
	}

	/* Compute new values */
	for (unsigned int j = 0; j < gen->val_size; j++) {
		out_val[j] = 0;
		for (unsigned int i = 0; i < gen->rows_size; i++) {
			if (gen->val[i] == NULL) continue;
			out_val[j] = ffadd(out_val[j], ffmul(gen->ctx, gen->val[i][j], temp_coef[i]));
		}
	}

	/* Compute new coefficients */
	for (unsigned int j = 0; j < gen->coef_size; j++) {
		out_coef[j] = 0;
		for (unsigned int i = 0; i < gen->rows_size; i++) {
			if (gen->coef[i] == NULL) continue;
			out_coef[j] = ffadd(out_coef[j], ffmul(gen->ctx, gen->coef[i][j], temp_coef[i]));
		}
	}

	free(temp_coef);
}

/* user shouldn't modify returned value */
uint8_t** gen_deliver(generation_t* gen, uint16_t* out_n) {
	unsigned int counter = 0;

	for (unsigned int i = 0; i < gen->rows_size; i++) {
		if (gen->delivered[i] || gen->coef[i] == NULL || gen->coef[i][i] != 1) continue;
		uint8_t found = 1;
		for (unsigned int j = 0; j < gen->coef_size; j++) {
			if (j == i) continue;
			if (gen->coef[i][j] != 0) {
				found = 0;
				break;
			}
		}

		if (found) {
			gen->val_deliver_buffer[counter] = gen->val[i];
			gen->delivered[i] = 1;
			gen->delivered_count += 1;
			counter++;
		}
	}

	*out_n = counter;
	return gen->val_deliver_buffer;
}

unsigned int gen_rank(generation_t* gen) {
	return gen->rows_size;
}

void gen_free(generation_t* gen) {
	for (unsigned int i = 0; i < gen->rows_size; i++) {
		free(gen->coef[i]);
		free(gen->val[i]);
	}
	free(gen->delivered);
	free(gen->val_deliver_buffer);

	free(gen->coef_buffer);
	free(gen->val_buffer);
	
	free(gen->coef);
	free(gen->val);
}
