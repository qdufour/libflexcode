from pyflexcode import lib, ffi

@ffi.def_extern()
def my_deliver_cb(handle, buf, length):
    return ffi.from_handle(handle).deliver(buf, length)

@ffi.def_extern()
def my_send_cb(handle, packets, length):
    return ffi.from_handle(handle).send(packets, length)

class Generation():
    def __init__(self):
        self.h = ffi.new_handle(self)
        self.poolc = ffi.gc(ffi.new("pool_t*"), lib.pool_free)
        lib.pool_init(self.poolc, self.h, lib.my_deliver_cb, lib.my_send_cb, 15, 8, 1, 29)

    def broadcast(self):
        lib.pool_broadcast(self.poolc, "test", 5)

    def receive(self):
        pkt = ffi.new("pool_packet_t*")
        pkt.gid = 4
        pkt.coefs_count = 1
        pkt.coefs = ffi.new("genkv_coef_t*")
        pkt.coefs.msg_id = 42
        pkt.coefs.coef_value = 1
        pkt.vals_count = 4
        pkt.vals = ffi.new("uint8_t[]", [65, 65, 65, 0])
        lib.pool_receive(self.poolc, pkt)

    def deliver(self, buf, length):
        print ffi.string(buf, length), length

    def send(self, packets, length):
        copied = []
        for i in range(length):
            pp = ffi.gc(ffi.new("pool_packet_t*"), lib.pool_packet_free)
            lib.pool_packet_deep_cpy(pp, ffi.addressof(packets, i))
            copied.append(pp)

        for pkt in copied:
            print ffi.unpack(pkt.vals, pkt.vals_count)

a = Generation()
a.broadcast()
a.receive()
