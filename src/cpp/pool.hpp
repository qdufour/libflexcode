#pragma once
extern "C" {
#include "../c/pool.h"
#include "../c/generation_kv.h"
}
#include <vector>

class Pool {
  protected:
    pool_t cpool;
    bool inited;
  public:
    Pool();
    void init(void* handle, deliver_fx_t deliver, send_fx_t send, uint8_t fanout, uint16_t max_rank, uint16_t split, uint16_t gen_hist_size, uint8_t pol, uint8_t ttl);
    ~Pool();

    uint32_t get_message_generation(uint32_t msg_id);
    void broadcast(unsigned char* msg, uint16_t msg_len);
    void broadcast(uint32_t msg_id, unsigned char* msg, uint16_t msg_len);
    void recode(pool_packet_t* pkt, uint32_t gid, uint8_t ttl);
    bool receive(pool_packet_t* pkt);
    genkv_t* get_gen(uint32_t gen);
    uint32_t current_generation();
    std::vector<unsigned int> stat_generations_size();
};
