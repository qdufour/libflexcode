#include "gf256.h"

unsigned char ffadd(unsigned char a, unsigned char b) { return a ^ b; }
unsigned char ffsub(unsigned char a, unsigned char b) { return a ^ b; }

unsigned char slow_ffmul(ctx_t* ctx, unsigned char a, unsigned char b) {
	unsigned char r = 0;

	while(a && b) {
		if (b & 1) r = r ^ a;
		
		// if a >= 128, then it will overflow when shiftled left, so reduce
		if (a & 0x80) a = (a << 1) ^ ctx->pol;
		else a <<= 1;

		b >>= 1;
	}

	return r;
}

void exp_table(ctx_t* ctx) {
	unsigned char generator = 1, correct = 0;

	while(!correct && generator != 0) {
		generator++;
		unsigned char unique[256] = { [0 ... 255] = 0 };
		unsigned char x = 0x01, index = 0;

		correct = 1;
		ctx->exp[index++] = x;

		for (unsigned char i=0; i < 255 && correct; i++) {
			unsigned char y = slow_ffmul(ctx, generator, x);
			ctx->exp[index++] = y;
			if (unique[y] > 0) correct = 0;
			unique[y] = 1;
			x = y;
		}
	}

	ctx->generator = generator;
}

void log_table(ctx_t* ctx) {
	for (unsigned char i = 0; i < 255; i++) ctx->log[ctx->exp[i]] = i;
}

void ffinit(ctx_t* ctx, unsigned char pol) {
	ctx->pol = pol;
	exp_table(ctx);
	log_table(ctx);

	#ifdef TRACE
	ctx->trace_fd = fopen("gf256-trace.out", "w+");
	if (ctx->trace_fd == NULL) {
		printf("Error - Unable to open file gf256-trace.out\n");
	}
	#endif
}

unsigned char ffmul(ctx_t* ctx, unsigned char a, unsigned char b) {
	int t;

	if (a == 0 || b == 0) return 0;
	
	t = ctx->log[a] + ctx->log[b];
	if (t > 255) t -= 255;
	
	return ctx->exp[t];
}

unsigned char ffdiv(ctx_t* ctx, unsigned char a, unsigned char b) {
	int t;
	if (a == 0 || b == 0) return 0;
  
	t = ctx->log[a] - ctx->log[b];
	if (t < 0) t += 255;
	
	return ctx->exp[t];
}


