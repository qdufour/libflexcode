#pragma once

/*
 * === Implement operations over Galois Field 2^8 (256) ===
 * Generally used for AES, here it's for network coding
 * Sources:
 * http://www.cs.utsa.edu/~wagner/laws/FFM.html
 * https://en.wikipedia.org/wiki/Finite_field_arithmetic
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define POL_AES    0x1b
#define POL_OCTAVE 0x1d
#define POL POL_OCTAVE
#define MAX(a, b) ( (a) < (b) ? (b) : (a) )

typedef struct {
	unsigned char pol;
	unsigned char generator;
	unsigned char exp[256];
	unsigned char log[256];
        FILE* trace_fd;
} ctx_t;

void ffinit(ctx_t* ctx, unsigned char pol);
unsigned char ffadd(unsigned char a, unsigned char b);
unsigned char ffsub(unsigned char a, unsigned char b);
unsigned char ffmul(ctx_t* ctx, unsigned char a, unsigned char b);
unsigned char ffdiv(ctx_t* ctx, unsigned char a, unsigned char b);
